import Base from './components/Base.jsx';
import HomePage from './components/HomePage.jsx';
import LoginPage from './components/LoginForm.jsx';
import SignUpPage from './components/SignUpForm.jsx';

const routes = {
  component: Base,
  childRoutes: [
    {
      path: '/',
      component: HomePage,
    },
    {
      path: '/login',
      component: LoginPage
    },
    {
      path: '/signup',
      component: SignUpPage
    }
  ]
};

export default routes;
