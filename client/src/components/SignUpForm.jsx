import React from 'react';
import {Link} from 'react-router';

class SignUpForm extends React.Component {

  /**
   * Class constructor.
   */
  constructor() {
    super();

    // set the initial component state
    this.state = {
      errorMessage: '',
      errors: {},
      user: {
        email: '',
        name: '',
        password: ''
      }
    };

    // this.processForm = this.processForm.bind(this);
    // this.changeUser = this.changeUser.bind(this);
  }

  /**
   * Process the form.
   *
   * @param {object} event - the JavaScript event object
   */
  processForm(event) {
    // prevent default action. in this case, action is the form submit event
    event.preventDefault();
    let self = this;
    let history = this.props.history;

    // create a string for an HTTP body message
    /* let user = 'name=' + encodeURIComponent(this.refs.name.getValue())
             + '&email=' + encodeURIComponent(this.refs.email.getValue())
             + '&password=' + encodeURIComponent(this.refs.password.getValue()); */

             console.log(this.state)
   let user = 'name=' + encodeURIComponent(this.state.user.name)
            + '&email=' + encodeURIComponent(this.state.user.email)
            + '&password=' + encodeURIComponent(this.state.user.password);
    // create an AJAX request
    let xhr = new XMLHttpRequest();
    xhr.open('post', '/auth/signup');
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.responseType = 'json';
    xhr.onload = function() {
      let state = {};

      if (this.status == 200) {
        // success

        state.errorMessage = '';
        state.errors = {};

        // change the component state
        self.setState(state);

        // set a message
        localStorage.setItem('successMessage', this.response.message);

        // change the current URL to /
        history.replaceState(null, '/login');
      } else {
        // failure

        state.errorMessage = this.response.message;
        state.errors = this.response.errors ? this.response.errors : {};

        // change the component state
        self.setState(state);
      }
    };
    xhr.send(user);
  }

  changeUser(event) {
    const field = event.target.name;
    console.log(field);
    const user = this.state.user;
    user[field] = event.target.value;


    this.setState({
      user
    });
  }


  /**
   * Render the component.
   */
  render() {
    return (
      <form action="/" onSubmit={this.processForm.bind(this)}>
        <h2>Sign Up</h2>
        {this.state.errorMessage && <p className="error-message">{this.state.errorMessage}</p>}

        <div className="field-line">
          <label>Name:</label>
          <input ref="name" className="form-control" onChange={this.changeUser.bind(this)} value={this.state.user.name} name="name" />
          {this.state.errors.name}
        </div>
        <div className="field-line">
          <label>Email:</label>
          <input type="text" ref="email" name="email" onChange={this.changeUser.bind(this)} value={this.state.user.email} className="form-control" ref="email" />
          {this.state.errors.email}
        </div>
        <div className="field-line">
          <label>Password:</label>
          <input ref="password" type="password" onChange={this.changeUser.bind(this)} name="password" className="form-control" value={this.state.user.password} />
          {this.state.errors.password}<br/>
        <div className="button-line">
            <button className="btn btn-default">Create New Account</button>
          </div>
          <Link to={'/login'}>Log in</Link>
        </div>

      </form>
    );
  }

}

export default SignUpForm;
