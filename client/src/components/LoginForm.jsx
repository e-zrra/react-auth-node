import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import Auth from '../modules/Auth';

class LoginForm extends React.Component {

  constructor () {
    super();

    this.state = {
      errorMessage: '',
      errors: {},
      user: {
        email: '',
        password: ''
      }
    };

    this.processForm = this.processForm.bind(this);
    // this.changeUser = this.changeUser.bind(this);
  }

  // @param {object} event - the Javascript event object
  processForm(event) {
    event.preventDefault();

    let self = this;
    let history = this.props.history;

    // create a string for an HTTP body message
    let user = 'email=' + encodeURIComponent(this.refs.email.getValue())
             + '&password=' + encodeURIComponent(this.refs.password.getValue());

    // create an AJAX request
    let xhr = new XMLHttpRequest();
    xhr.open('post', '/auth/login');
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.responseType = 'json';
    xhr.onload = function() {
     let state = {};

     if (this.status == 200) {
       // success

       state.errorMessage = '';
       state.errors = {};

       // change the component state
       self.setState(state);

       // save the token
       Auth.authenticateUser(this.response.token);

       // change the current URL to /
       history.replaceState(null, '/');
     } else {
       // failure

       state.errorMessage = this.response.message;
       state.errors = this.response.errors ? this.response.errors : {};

       // change the component state
       self.setState(state);

     }
   };
   xhr.send(user);
  }

  /**
   * Render the component.
   */
  render() {
    return (
      <form action="/" onSubmit={this.processForm}>
        <h2>Login</h2>

        {this.state.errors.summary && <p className="error-message">{this.state.errors.summary}</p>}

        <div className="field-line">
          <label>Email:</label>
          <input type="text" name="email" className="form-control" />
          {this.state.errors.email}
          <label>Password:</label>
          <input type="password" name="password" className="form-control" />
          {this.state.errors.password}
        </div>
        <div className="button-line">
          <button className="btn btn-default">Log in</button>
        </div>
        <Link to={'/signup'}>Create one account</Link>
      </form>
    )
  }

}

export default LoginForm;
