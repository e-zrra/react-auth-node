import React from 'react';
import {Link} from 'react-router';
import Auth from '../modules/Auth';

class Dashboard extends React.Component {
  // Class constructor

  constructor () {
    super();
    this.state = {
      secretData: ''
    };
  }

  // This method will be executed after the initial rendering
  componentDidMount () {
    let self = this;

    let xhr = new XMLHttpRequest();
    xhr.open('get', '/api/dashboard');
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    // set the authorization HTTP header
    xhr.setRequestHeader('Authorization', 'bearer ' + Auth.getToken());
    xhr.responseType = 'json';
    xhr.onload = function() {
      let state = {};

      if (this.status == 200) {
        state.secretData = this.response.message;
        self.setState(state);
     }

    };
    xhr.send();
  }

  render () {
    return (
      <div>
        <h1>Dashboard</h1>
        {this.state.secretData} - {this.state.secretData}
      </div>
    )
  }

}

export default Dashboard;
